<?php

	$feature_slider_active = get_field( 'feature_slider_active' );
	$form_active           = get_field( 'form_active' );

?>

<?php

if ( have_rows( 'feature_slider' ) && $feature_slider_active == 1 ) :
	?>
		<div class="feature-slider-wrap d-flex align-items-center">
			<div class="feature-slider">
				<?php
				while ( have_rows( 'feature_slider' ) ) :
					the_row();
					$image = get_sub_field( 'image' );
					?>
						<div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
						<?php
					endwhile;
				?>
			</div>

				<div class="overlay_wrap py-4 w-100">
					<div class="overlay container">
						<div>
							<?php 
							$feature_slider_content = get_field('feature_slider_content');
							if ( $feature_slider_content ) : 
							?>
							<?php echo $feature_slider_content; ?>
							<a class='btn btn-primary' href="/contact/">Contact Us</a>
							<?php endif; ?>
							
						</div>
						<?php if ( $form_active ) : ?>
						<div class='top-banner-form d-none d-md-block'>
							<?php echo do_shortcode( '[gravityform id=1 title=false description=false ajax=true tabindex=49]' ); ?>
						</div>
						<?php endif; ?>
					</div>
				</div>

		</div>
	<?php
else :
	// no rows found
endif;

?>
