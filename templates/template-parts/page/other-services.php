<div class="py-4 our-products">
	<div class="container">
		<h3 class="h2 text-center mb-4">Other Railings</h3>

		<?php
			$current_product_id = $post->ID;
		?>

		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'service',
		    );

		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<div class="our-product-loop row">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    	$thumbnail = get_the_post_thumbnail_url();
		    ?>
		    	
		    	<?php if($current_product_id != $post->ID): ?>
			        <div class="text-center col-sm-6 col-md-6 col-lg-4 my-2 px-sm-4">
			        	<img src="<?php echo $thumbnail; ?>" class="img-full">
			        	<p class="my-2"><?php echo $title; ?></p>
			        	<a href="<?php echo $link; ?>" class="btn btn-sm btn-secondary">Read More</a>
			        </div>
			    <?php endif; ?>

				<?php
		        endwhile;
		        ?>
		        </div>
		    <?php

		    endif; // End Loop

		    wp_reset_query();
		?>

	</div>
</div>