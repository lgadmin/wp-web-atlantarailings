

<div class="py-4 our-products">
	<div class="container">
		<h3 class="h2 text-center text-white mb-4">Our Services</h3>

		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'service',
		    );

		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<div class="our-product-loop row">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    	$thumbnail = get_the_post_thumbnail_url();
		    ?>
		    	
		        <div class="text-center text-white col-sm-6 col-md-6 col-lg-3 my-2 px-sm-4">
		        	<img src="<?php echo $thumbnail; ?>" class="img-full">
		        	<p class="my-2"><?php echo $title; ?></p>
		        	<a href="<?php echo $link; ?>" class="btn btn-sm btn-secondary">Read More</a>
		        </div>

				<?php
		        endwhile;
		        ?>
		        </div>
		    <?php

		    endif; // End Loop

		    wp_reset_query();
		?>

	</div>
</div>