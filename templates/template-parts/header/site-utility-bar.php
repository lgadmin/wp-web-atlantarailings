<div class="utility-bar">
  <div class="">
    <div class="left">
    <nav class="navbar navbar-default navbar-expand-md">  
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <?php 

                $utility = array(
                  'theme_location'          => 'utility-nav',
                  'depth'             => 1,
                  'container'         => 'div',
                  'container_class'   => 'collapse navbar-collapse',
                  'container_id'      => 'utility-navbar',
                  'menu_class'        => 'nav navbar-nav',
                  'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                    // 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
                  'walker'            => new WP_Bootstrap_Navwalker()
                );
                wp_nav_menu($utility);

              ?>
          </nav>
      

    </div>
    <div class="right">
    <?php echo do_shortcode('[lg-social-media]'); ?>
    </div>
  </div>
</div>