<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<div class="py-4 text-center container">
					<h1 class="h2 text-primary">Locations</h1>
					<p>Below is a list of dealers that can help with the install of your Topless Glass Railings System from Falcon Railings USA.</p>
					<p>If you would like to become a Falcon Railings dealer, please fill out our <a href="/request-dealer-installers/">Request for Dealer/Installers Form</a>.</p>
				</div>

				<hr class="container m-0">

				<div class="py-4">
					<?php get_template_part("/templates/template-parts/page/location-list"); ?>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>