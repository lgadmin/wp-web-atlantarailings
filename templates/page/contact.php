<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				<div class="container">
					<div class="row">
						<div class="col-md-7 py-4 justify-content-center">
						<?php get_template_part( '/components/acf-flexible-layout/layouts' ); ?>
						
						</div>
						<div class="col-md-1">
							&nbsp;
						</div>
						<div class="col-md-4 py-4">
							<div class="pt-4 flexible-content">
								<div class="col-12 ">
									<?php get_sidebar(); ?>
								</div>
							</div>
						</div>
					</div>
<!-- 					<div class="row justify-content-between">
	<div class="col-md-7">
		
	</div>
	<div class="col-md-4">
		
	</div>
</div> -->
				</div>
			</main>
		</div>
	</div>

<?php get_footer(); ?>