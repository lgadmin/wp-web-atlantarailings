<?php

	include 'elements/table.php';
	include 'elements/spacing.php';
	include 'elements/font-size.php';
	include 'elements/list.php';
	include 'elements/image.php';
	include 'elements/color.php';

	// Register format button
	function my_mce_buttons_2( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


	// Insert items to format button
	function lg_mce_before_init_insert_formats( $init_array ) { 

		global $lg_tinymce_table;
		global $lg_tinymce_margin_top;
		global $lg_tinymce_margin_bottom;
		global $lg_tinymce_font_size;
		global $lg_tinymce_list;
		global $lg_tinymce_image;
		global $lg_tinymce_text_color;
		global $lg_tinymce_button;

	    $style_formats = array(
	    	$lg_tinymce_font_size,
	    	$lg_tinymce_text_color,
			$lg_tinymce_button,
			$lg_tinymce_image,
			$lg_tinymce_list,
	        $lg_tinymce_table,
	        $lg_tinymce_margin_top,
	        $lg_tinymce_margin_bottom
	    );

	    $init_array['style_formats'] = json_encode( $style_formats );

	    return $init_array; 
	  
	} 

	add_filter( 'tiny_mce_before_init', 'lg_mce_before_init_insert_formats' );  

?>