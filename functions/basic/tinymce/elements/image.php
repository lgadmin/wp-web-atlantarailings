<?php

	global $lg_tinymce_image;
	
	$lg_tinymce_image = array(
	    'title' => 'Image',
	    'items' =>  array(
	    	array(
				'title' => 'Full',
	            'selector' => 'img',
	            'classes' => 'img-full'
			),
			array(
				'title' => 'Thumbnail',
	            'selector' => 'img',
	            'classes' => 'img-thumbnail'
			),
			array(
				'title' => 'Rounded',
	            'selector' => 'img',
	            'classes' => 'rounded'
			),
	    )
	)

?>