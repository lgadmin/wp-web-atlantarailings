<?php

// Admin Screen Filters

/*
add_action('restrict_manage_posts', 'service_filter_by_category');

function service_filter_by_category() {
  global $typenow;
  $post_type = 'service'; // change to your post type
  $taxonomy  = 'service-category'; // change to your taxonomy
  if ($typenow == $post_type) {
	$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
	$info_taxonomy = get_taxonomy($taxonomy);
	wp_dropdown_categories(array(
	  'show_option_all' => __("Show All {$info_taxonomy->label}"),
	  'taxonomy'        => $taxonomy,
	  'name'            => $taxonomy,
	  'orderby'         => 'name',
	  'selected'        => $selected,
	  'show_count'      => true,
	  'hide_empty'      => true,
	));
  };
}

add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
  global $pagenow;
  $post_type = 'service'; // change to your post type
  $taxonomy  = 'service-category'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
	$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
	$q_vars[$taxonomy] = $term->slug;
  }
}*/

add_action( 'acf/init', 'my_acf_add_local_field_groups' );

function title_shortcode() {
	return get_the_title();
}
add_shortcode( 'page_title', 'title_shortcode' );

add_filter( 'wp_nav_menu_items', 'mobile_additional_items', 10, 2 );
function mobile_additional_items( $items, $args ) {
	if ( $args->theme_location == 'top-nav' ) {
		$items .= '<div class="d-md-none additional-menu-items"> <div class="solid-line"> </div>';
    $items .= '<ul class="mobile-utility nav navbar-nav"> ';
		$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-service nav-item"> <a class="nav-link" href="/about-us/"> About Us </a> </li>';
		$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-service nav-item"> <a class="nav-link" href="/faq/">FAQ </a> </li>';
		$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-service nav-item"> <a class="nav-link" href="/contact/">Contact Us</a> </li>'  ;
		$items .= do_shortcode( '[lg-social-media]' );
    $items .= '</ul>';
    $items .= '</div>';
    

	}
	return $items;
}




