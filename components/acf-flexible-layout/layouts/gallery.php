<?php 
/**
 * Gallery Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

$gallery_images = get_sub_field('gallery_images');
$gallery_title = get_sub_field('gallery_title');
$size = 'full'; // (thumbnail, medium, large, full or custom size)

if( $gallery_images ): ?>
    <?php 
    if($gallery_title){
        echo '<h2 class="pb-4 w-100 text-center">' . $gallery_title . '</h2>';
    }
    ?>
    <div class="gallery-grid">
        <?php foreach( $gallery_images as $image ): ?>
            <div class="grid-item">
                <a data-lightbox="glass-railing" href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
                
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
