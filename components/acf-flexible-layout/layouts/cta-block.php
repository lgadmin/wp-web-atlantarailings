<?php 

	$cta_title = get_sub_field('cta_title');
	$cta_button = get_sub_field('cta_button');
	include(locate_template('/templates/template-parts/footer/cta-block.php')); 

?>